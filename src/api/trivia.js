import { TRIVIA_HOST_URL as BASE_URL } from "./index.js";

const apiURL = BASE_URL;
const apiKey = "random";

/**
 * Registers user with username
 * @param {String} username
 * @returns {Object} - returns a user object or undefined
 */
export async function registerUser(username) {
  const userId = await fetch(`${apiURL}/trivia`, {
    method: "POST",
    headers: {
      "X-API-Key": apiKey,
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      username: username,
      highScore: 0,
    }),
  })
    .then((response) => {
      if (!response.ok) {
        throw new Error("Could not create new user");
      }
      return response.json();
    })
    .then((newUser) => {
      // newUser is the new user with an id
      return newUser;
    })
    .catch((error) => {
      return error;
    });

  return userId;
}

/**
 * Fetches user from API with username
 * @param {String} username
 * @returns {Object} - returns a user object.
 */
export async function getUser(username) {
  const user = await fetch(`${apiURL}/trivia?username=${username}`)
    .then((response) => response.json())
    .then((results) => {
      return results;
    })
    .catch((error) => {
      console.log(error);
      return [];
    });

  return user;
}
/**
 * Updates users highscore
 * @param {String} userId - Id of user
 * @param {Number} highScore
 * @returns - returns a new user Object or undefined if no user was found
 */
export async function patchUser(userId, highScore = 0) {
  const updatedUser = await fetch(`${apiURL}/trivia/${userId}`, {
    method: "PATCH", // NB: Set method to PATCH
    headers: {
      "X-API-Key": apiKey,
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      // Provide new highScore to add to user with id 1
      highScore: highScore,
    }),
  })
    .then((response) => {
      if (!response.ok) {
        throw new Error("Could not update high score");
      }
      return response.json();
    })
    .then((updatedUser) => {
      // updatedUser is the user with the Patched data
      return updatedUser;
    })
    .catch((error) => {
      console.log(error);
      return null;
    });

  return updatedUser;
}

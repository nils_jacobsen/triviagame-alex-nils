import axios from "axios";
import { OPEN_TRIVIA_DATABASE_HOST_URL as BASE_URL } from "./index.js";

const SUCCESS = 0;
/**
 * Fetches categories from API
 * @returns {Array} - returns an array containing category objects or undefined if no category was found
 */
export async function getCategories() {
  const path = "/api_category.php";
  const url = BASE_URL + path;
  try {
    const response = await axios.get(url);
    const data = response.data;
    const categories = data.trivia_categories;
    return categories;
  } catch (error) {
    console.log(error);
  }
}
/**
 * Fetches question from API with query params
 * @param {Object} params - takes a object with key-value pairs
 * @returns {Array} returns an array with questions objects
 */
export async function getQuestions(params = { amount: 10 }) {
  const path = `/api.php`;
  const url = BASE_URL + path;
  // Remove parameters that has "any" values
  const newParams = Object.fromEntries(
    Object.entries(params).filter(([_, value]) => value !== "any")
  );
  try {
    const response = await axios.get(url, {
      params: { ...newParams },
    });
    const data = response.data;
    const { response_code, results } = data;
    switch (response_code) {
      case SUCCESS:
        return results;
      default:
        throw new Error("Response_code: " + response_code);
    }
  } catch (error) {
    console.log(error);
  }
}

import { createStore } from "vuex";
import {
  getQuestions as fetchQuestions,
  getCategories,
} from "./api/openTriviaDataBase";
import { getUser, registerUser, patchUser } from "./api/trivia";

const initialState = {
  categories: [],
  questions: [],
  answers: [],
  user: {},
  index: null,
};

export default createStore({
  state: { ...initialState },
  getters: {
    getSelectedQuestion: (state) => {
      return state.questions[state.index];
    },
    getQuestions: (state) => {
      return state.questions;
    },
    getCategories: (state) => {
      return state.categories;
    },
    getScore: (state) => {
      const { questions, answers } = state;
      let score = 0;
      const scoreMultiplier = 10;
      // Calculate score
      for (let i = 0; i < questions.length; i++) {
        if (questions[i].correct_answer === answers[i])
          score += scoreMultiplier;
      }
      return score;
    },
    getHighScore: (state) => state.user.highScore,
    getAnswers: (state) => state.answers,
    hasQuestions: (state) => state.questions.length > 0,
    hasNextQuestion: (state) => state.questions.length - 1 >= state.index,
    getProgress: (state) =>
      Math.round((state.index / state.questions.length) * 100),
  },
  mutations: {
    setQuestions(state, questions) {
      state.questions = questions.map((question, index) => {
        question.id = index + 1;
        return question;
      });
      state.index = 0;
    },
    setCategories(state, categories) {
      // sort categories by name in ascending order
      const sortedCategories = categories.sort((categoryA, categoryB) => {
        const nameA = categoryA.name.toUpperCase(); // ignore upper and lowercase
        const nameB = categoryB.name.toUpperCase(); // ignore upper and lowercase
        if (nameA < nameB) {
          return -1;
        }
        if (nameA > nameB) {
          return 1;
        }

        // names must be equal
        return 0;
      });
      console.log(sortedCategories);
      state.categories = sortedCategories;
    },
    setAnswers(state, answers) {
      state.answers = answers;
      state.questions = state.questions.map((question, index) => {
        question.answer = answers[index];
        // check if answer is correct
        question.isCorrect = question.correct_answer === answers[index];
        return question;
      });
    },
    setAnswer(state, answer) {
      state.answers[state.index] = answer;
      state.questions[state.index].answer = answer;
      state.questions[state.index].isCorrect =
        state.questions[state.index].correct_answer === answer;
    },
    setUser(state, user) {
      state.user = user;
    },
    setHighScore(state, highscore) {
      state.user.highScore = highscore;
    },
    nextQuestion(state) {
      state.index++;
    },
    resetState(state) {
      state = { ...initialState };
    },
  },
  actions: {
    async loadQuestions(context, payload) {
      console.log(payload);
      const newQuestions = await fetchQuestions(payload);
      const modifiedQuestions = newQuestions.map((question) => {
        return { ...question, answer: "" };
      });
      context.commit("setQuestions", modifiedQuestions);
    },
    async loadCategories(context) {
      const categories = await getCategories();
      context.commit("setCategories", categories);
    },
    setAnswers(context, payload) {
      context.commit("setAnswers", payload);
    },
    setAnswer(context, payload) {
      context.commit("setAnswer", payload);
    },

    async loadUser(context, username) {
      // check if user exists in the database
      let user = await getUser(username);
      if (user.length > 0) {
        // Return the first user
        return user[0];
      } else {
        // Register user
        user = await registerUser(username);
        console.log("register user:", user);
      }
      context.commit("setUser", user);
    },
    async updateUser(context) {
      const user = context.state.user;
      return patchUser(user.id, user.highScore);
    },

    async startGame(context, payload) {
      await context.dispatch("loadQuestions", payload.options);
      await context.dispatch("loadUser", payload.username);
    },
    async endGame(context) {
      // compare highscore and score
      const score = this.getters.getScore;
      const highscore = this.getters.getHighScore;
      if (score > highscore) {
        context.commit("setHighScore", score);
      }
      return await context.dispatch("updateUser");
    },
    async resetGame(context) {
      context.commit("resetState");
    },
    async nextQuestion(context) {
      if (context.state.index === context.state.questions.length - 1)
        return false;
      else context.commit("nextQuestion");
      return true;
    },
  },
});

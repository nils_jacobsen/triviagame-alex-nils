<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">Trivia Game</a>
      <ul>
      <li><a href="#component-feature-tree">Component Feature Tree</a></li>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#contact">Contact</a></li>
  </ol>
</details>

<!-- About the project -->

## Trivia Game

Link to demo: [Trivia Game Demo](https://trivia-game-alex-nils.herokuapp.com/)
Trivia game that lets you choose category, difficulty and how many questions.
Your chosen username keeps track of your highscore - every correct answer gives you 10 points.

[Requirements doc](./Vue_Trivia%20Game.pdf)

### Component Feature Tree

<details>
<summary>picture of component feature tree</summary>
![Image of the component feature tree](./component-feature-tree.png)
</details>

<p align="right">(<a href="#top">back to top</a>)</p>

### Built With

- [Vue.js](https://vuejs.org/)
- [vue-router](https://router.vuejs.org/)
- [vuex](https://vuex.vuejs.org/)
- [vite](https://vitejs.dev/)
- [Bootstrap 5.1.3](https://getbootstrap.com/docs/5.1/getting-started/introduction/)

Dependencies are listed in [package.json](./package.json)

<p align="right">(<a href="#top">back to top</a>)</p>

<!-- GETTING STARTED -->

## Getting Started

### Prerequisites

[VSCode](https://code.visualstudio.com/) or any other editor.
[NodeJS](https://nodejs.org/en/)

### Installation

1. Clone the repo
   ```sh
   git clone https://gitlab.com/nils_jacobsen/triviagame-alex-nils.git
   ```
2. Enter the cloned project folder
   ```sh
   cd triviagame-alex-nils
   ```
3. Install NPM packages
   ```sh
   npm install
   ```
4. Run the app
   ```js
   npm run dev
   ```

<p align="right">(<a href="#top">back to top</a>)</p>

## Usage

Optional

1. Enter username
2. Enter number of questions
3. Enter difficulty
4. Enter category

How to play

1. Click on START button
2. Choose an answer by clicking on a button

Check highscore
1. Enter link with all user highscores: [database](https://noroff-assignment-api-shuhia.herokuapp.com/trivia)

<p align="right">(<a href="#top">back to top</a>)</p>

<!-- CONTACT -->

## Contact

[Alex On](https://www.linkedin.com/in/alex-on-0a08b8107/)

[Nils Jacobsen](https://www.linkedin.com/in/nilsedgarjacobsen/)

<p align="right">(<a href="#top">back to top</a>)</p>
